%undefine __cmake_in_source_build

%global gettext_package FontForge
%global gittag0 20230101

Name:           fontforge
Version:        20230101
Release:        4
Summary:        Outline and bitmap font editor
License:        GPL-3.0-or-later
URL:            https://fontforge.github.io/
Source0:        https://github.com/fontforge/%{name}/archive/%{gittag0}.tar.gz#/%{name}-%{version}.tar.gz
Patch0:         Fix-errors-in-French-and-Italian-translations.patch
# https://github.com/fontforge/fontforge/commit/216eb14b558df344b206bf82e2bdaf03a1f2f429
Patch1:         CVE-2024-25081_CVE-2024-25082.patch

Requires:       xdg-utils potrace hicolor-icon-theme
BuildRequires:  gcc-c++ cmake libjpeg-devel libtiff-devel libpng-devel giflib-devel libxml2-devel
BuildRequires:  freetype-devel desktop-file-utils libuninameslist-devel libXt-devel xorg-x11-proto-devel
BuildRequires:  gettext pango-devel cairo-devel libspiro-devel python3-devel readline-devel
BuildRequires:  libappstream-glib woff2-devel shared-mime-info gtk3-devel python3-sphinx

%description
FontForge (former PfaEdit) is a font editor for outline and bitmap
fonts. It supports a range of font formats, including PostScript
(ASCII and binary Type 1, some Type 3 and Type 0), TrueType, OpenType
(Type2) and CID-keyed fonts.

%package devel
Summary: Development files for fontforge
Requires: %{name} = %{version}-%{release}
Requires: %{name}-help = %{version}-%{release}

%description devel
This package includes the library files you will need to compile
applications against fontforge.

%package help
Summary: Documentation files for %{name}
BuildArch: noarch
Provides:  %{name}-doc = %{version}-%{release}
Obsoletes: %{name}-doc < %{version}-%{release}

%description help
This package contains documentation files for %{name}.

%prep
%autosetup -n %{name}-%{version} -p1

# Remove tests that requires Internet access
sed -i '45d;82d;101d;127d' tests/CMakeLists.txt


%build
export CFLAGS="%{optflags} -fno-strict-aliasing"

%cmake -DCMAKE_BUILD_TYPE=Release \
          -DENABLE_WOFF2=ON \
          -DENABLE_DOCS=NO

%{cmake_build}

%install
%{cmake_install}


desktop-file-install \
  --dir $RPM_BUILD_ROOT%{_datadir}/applications            \
  --add-category X-Fedora                                  \
  desktop/org.fontforge.FontForge.desktop

rm -f %{buildroot}%{_datadir}/doc/fontforge/{.buildinfo,.nojekyll}
appstream-util validate-relax --nonet %{buildroot}%{_metainfodir}/*.appdata.xml

# Find translations
%find_lang %{gettext_package}

%check
pushd %{__cmake_builddir}
make check
popd

%files -f %{gettext_package}.lang
%doc AUTHORS
%license LICENSE COPYING.gplv3
%{_bindir}/*
%{_libdir}/lib*.so.*
%{_datadir}/applications/*FontForge.desktop
%{_datadir}/fontforge
%{_datadir}/icons/hicolor/*/apps/org.fontforge.FontForge*
%{_datadir}/mime/packages/fontforge.xml
%{_metainfodir}/org.fontforge.FontForge.appdata.xml
%{python3_sitearch}/fontforge.so
%{python3_sitearch}/psMat.so

%files devel
%{_libdir}/lib*.so

%files help
%doc %{_pkgdocdir}
%{_mandir}/man1/*.1*

%changelog
* Fri Nov 08 2024 Funda Wang <fundawang@yeah.net> - 20230101-4
- adopt to new cmake macro
- force out-of-source build

* Tue Feb 27 2024 yaoxin <yao_xin001@hoperun.com> - 20230101-3
- Fix CVE-2024-25081 and CVE-2024-25082

* Wed Jul 19 2023 yaoxin <yao_xin001@hoperun.com> - 20230101-2
- Fix build error caused by gettext update to 0.22

* Mon Apr 24 2023 liyanan <thistleslyn@163.com> - 20230101-1
- upgrade 20230101

* Sat Nov 12 2022 hua <dchang@zhixundn.com> 20220308-1
- update to 20220308

* Tue Jul 13 2021 wangyue <wangyue92@huawei.com> - 20200314-6
- remove useless glib buildrequire
- move %find_lang to correct stage

* Tue Jun 15 2021 baizhonggui <baizhonggui@huawei.com> - 20200314-5
- Fix building error: Could not open %files file /home/abuild/rpmbuild/BUILD/fontforge-20200314/FontForge.lang:No such file or directory
- Add glib in BuildRequires

* Fri May 21 2021 wutao <wutao61@huawei.com> - 20200314-4
- update release

* Wed Dec 16 2020 Ge Wang <wangge20@huawei.com> - 20200314-2
- change install require autotrace to potrace

* Sat Aug 1 2020 xiaoweiwei <xiaoweiwei5@huawei.com> - 20200314-1
- upgrade to 20200314

* Mon Jun 22 2020 yanan li <liyanan032@huawei.com> - 20170731-13
- Solve fontforge 20170731 build fails with python3.8.

* Fri Apr 3 2020 zhujunhao <zhujunhao5@huawei.com> - 20170731-12
- Modify build option

* Thu Dec 12 2019 lihao <lihao129@huawei.com> - 20170731-11
- Package Init

